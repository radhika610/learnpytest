import pytest

from Calculator import Calculator


def testMultiplicationwithNumbers():
    cal = Calculator()
    Res = cal.Multiplication(20, 30)
    assert Res == 600


def testMultiplicationWithPositiveNumbers():
    cal = Calculator()
    Res = cal.Multiplication(10,20)
    assert Res == 200

@pytest.mark.parametrize("num,output", [(1, 11), (2, 22), (3, 33), (4, 44)])
def test_multiplication_11(num, output):
    assert 11 * num == output
