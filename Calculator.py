class Calculator:
    def Addition(self,num1,num2):
        result=num1+num2
        return result

    def Division(self,num1,num2):
        try:
            result=num1/num2
            return result
        except ZeroDivisionError:
            return "You can not divide by zero"

    def Multiplication(self,num1,num2):
        result = num1 * num2
        return result

    def Subtraction(self,num1,num2):
        result = num1-num2
        return result
