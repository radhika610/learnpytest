from Calculator import Calculator

global cal


def testAdditionWithPositiveNumbers(CreateObject):
    # cal = Calculator()
    Res = CreateObject.Addition(10, 20)
    assert Res == 30


def testAdditionWithPositiveNumbers1(SendPositiveNumbers):
    cal = Calculator()
    Res = cal.Addition(SendPositiveNumbers[0], SendPositiveNumbers[1])
    assert Res == SendPositiveNumbers[2]


def testAdditionWithOnePositiveandOneNegative(SendPositiveNumbersUsingDictionary):
    cal = Calculator()
    Res = cal.Addition(SendPositiveNumbersUsingDictionary["Number1"], SendPositiveNumbersUsingDictionary["Number2"])
    assert Res == SendPositiveNumbersUsingDictionary["Result"]


def testAdditionWithZeroNumbers(CreateObject):
    # cal = Calculator()
    Res = CreateObject.Addition(0, 0)
    assert Res == 0
