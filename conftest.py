import pytest

from Calculator import Calculator


@pytest.fixture
def input_value():
    input = 39
    return input


@pytest.fixture()
def CreateObject():
    # global cal
    cal = Calculator()
    return cal


@pytest.fixture()
def PositiveNumbers():
    print("Sending positive numbers")
    # returning list of numbers
    return [1000, 200, 250]

#fixture using tuple
@pytest.fixture(params=[(1000, 2000, 3000), (5, 1, 6), (10000, 20000, 30000)])
def SendPositiveNumbers(request):
    # return request.param returns single tuple from params with each iteration
    return request.param

# fixtures using dictionary
@pytest.fixture(params=[{"Number1": 3000, "Number2": -1000, "Result": 2000}, {"Number1": 1, "Number2": -1, "Result": 0},
                        {"Number1": -40, "Number2": 45, "Result": 5}])
def SendPositiveNumbersUsingDictionary(request):
    # return request.param returns single set of dictionary  from params with each iteration
    return request.param
