import pytest

from Calculator import Calculator


def testDivisionWithPositiveNumbers(CreateObject):
    #cal = Calculator()
    Res = CreateObject.Division(100, 20)
    assert Res == 5

def testDivisionWithZeroNumbers(CreateObject):
    #cal = Calculator()
    Res = CreateObject.Division(100, 0)
    assert Res == "You can not divide by zero"
